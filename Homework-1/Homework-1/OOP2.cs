﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework_1
{
    class OOP2
    {
        public void TestCode(PhoneBook phoneBook)
        {
            phoneBook.AddContact("armani", "0587155571");
            phoneBook.AddContact("grmani", "0587155571");
            phoneBook.AddContact("ali", "0587155571");


            phoneBook.DelContact("anna");

            Console.WriteLine(phoneBook.GetPhone("ali"));

            string[] Names = phoneBook.GetAllContactsNames();
            foreach (var name in Names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine(phoneBook.ToString());
        }
        public class PhoneBook
        {
            public Node<Contact> Contacts { get; set; }

            public PhoneBook(Node<Contact> contacts)
            {
                Contacts = contacts;
            }

            public void AddContact(string name, string phone)
            {
                Node<Contact> temp = Contacts;
                bool exists = false;
                while (temp != null)
                {
                    if (temp.GetValue().Name == name)
                    {
                        temp.GetValue().Phone = phone;
                        exists = true;
                    }

                    temp = temp.GetNext();
                }
                if (!exists)
                {

                    Contacts.AddValue(0, new Contact(name, phone));

                }
            }

            public void DelContact(string name)
            {
                Node<Contact> temp = Contacts;
                int i = 0;
                while (temp!= null && i <= (Contacts.GetSize() - 2))
                {
                    
                    if (i == 0 && temp.GetValue().Name == name)
                    {
                        Contacts = Contacts.GetNext();
                        break;
                    }

                    else if ((i == Contacts.GetSize() - 2) && (temp.GetNext().GetValue().Name == name))
                    {
                        temp.SetNext(null);
                        break;
                    }

                    else if ((temp.GetNext().GetValue().Name == name) && (i < Contacts.GetSize() - 2))
                    {
                        temp.SetNext(temp.GetNext().GetNext());
                        break;
                    }



                    i++;
                    temp = temp.GetNext();
                }
            }

            public string GetPhone(string name)
            {
                Node<Contact> temp = Contacts;
                while (temp != null)
                {
                    if (temp.GetValue().Name == name)
                    {
                        return temp.GetValue().Phone;
                    }
                    temp = temp.GetNext();
                }
                return null;

            }

            public string[] GetAllContactsNames()
            {
                if (Contacts != null)
                {
                    string[] Names = new string[Contacts.GetSize()];
                    Node<Contact> temp = Contacts;
                    int i = 0;
                    while (temp != null)
                    {
                        Names[i] = temp.GetValue().Name;
                        i++;
                        temp = temp.GetNext();
                    }
                    return Names;
                }
                return new string[0];
            }

            public override string ToString()
            {
                Contact[] contacts = new Contact[Contacts.GetSize()];
                Node<Contact> temp = Contacts;
                int i = 0;
                while (temp != null)
                {
                    contacts[i] = temp.GetValue();
                    temp = temp.GetNext();
                    i++;
                }
                Array.Sort(contacts, (x, y) => string.Compare(x.Name, y.Name));

                string finalStr = "";
                foreach (var contact in contacts)
                {
                    finalStr += $"{contact.Name}   {contact.Phone}\n";
                }
                return finalStr;



            }
        }

        public class Contact
        {
            public string Name { get; set; }
            public string Phone { get; set; }

            public Contact(string name, string phone)
            {
                Name = name;
                Phone = phone;
            }
        }
    }
}
