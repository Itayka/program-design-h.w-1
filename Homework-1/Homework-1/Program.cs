﻿using System;
using System.Collections.Generic;

namespace Homework_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //_Node2();

            //_Node48();

            //_Node53();

            //_Node14();

            //_Stack18();

            //_Stack1();

            //_Stack14();

            //_Stack15();

            //_Stack16();

            //_Stack17();

             _OOP1();

            //_OOP2();
        }

        private static void _Node2()
        {
            Node2 task2 = new Node2();
            Data data = Data.Instance;
            Node<Node2.RangeNode> rangeList = task2.CreateRangeList(data.Sequence);

            Node<Node2.RangeNode> tmp = rangeList;
            string finalString = "";

            while (tmp != null)
            {
                int from = tmp.GetValue().From;
                int to = tmp.GetValue().To;
                finalString += $"({from},{to}) -->";
                tmp = tmp.GetNext();
            }
            finalString += "Null";
            Console.WriteLine(finalString);

        }

        private static void _Node48()
        {
            Node48 task48 = new Node48();

            Data data = Data.Instance;

            Console.WriteLine(task48.GetRepresentation(data.Representation).GetString());
        }

        private static void _Node53()
        {

            Node53 task53 = new Node53();

            Data data = Data.Instance;

            string result1 = task53.GetStudentsAvg(data.GradeDB);
            Console.WriteLine(result1);

            Console.WriteLine("");

            KeyValuePair<int, double> result2 = task53.GetBestAvgCourse(data.GradeDB);
            Console.WriteLine($"Best average course: {result2.Key}; Avarge: {result2.Value}");

        }

        private static void _Node14()
        {
            Node14 task14 = new Node14();
            Data data = Data.Instance;
            Console.WriteLine(task14.BiggestBuildings(data.Buildings));



        }

        private static void _Stack18()
        {
            Data data = Data.Instance;
            Stack18 stack18 = new Stack18();
            stack18.Control(data.stack18);
        }

        private static void _Stack1()
        {
            Data data = Data.Instance;
            Stack1 stack1 = new Stack1();
            Console.WriteLine(stack1.GetSumOfMaxPair(data.stack1_1, data.stack1_2));
        }

        private static void _Stack14()
        {
            Data data = Data.Instance;
            Stack14 stack14 = new Stack14();
            Console.WriteLine(stack14.GetSeq(data.stack14, 876));
        }

        private static void _Stack15()
        {
            Data data = Data.Instance;
            Stack15 stack15 = new Stack15();
            stack15.arrangeStack(data.stack15);
            Console.WriteLine(data.stack15);
        }

        private static void _Stack16()
        {
            Data data = Data.Instance;
            Stack16 stack16 = new Stack16();
            Console.WriteLine(stack16.Decode(data.encodedString16));

        }

        private static void _Stack17()
        {
            Data data = Data.Instance;
            Stack17 stack17 = new Stack17();
            Console.WriteLine(stack17.GetLongestSeq(data.stack17));
        }

        private static void _OOP1()
        {
            Data data = Data.Instance;
            OOP1 OOP1 = new OOP1();
            OOP1.PrintAvailableDishes(data.OOP1Menu, data.MissingIngredientOOP1);


        }

        private static void _OOP2()
        {
            Data data = Data.Instance;
            OOP2 OOP2 = new OOP2();
            OOP2.TestCode(data.phoneBook);


        }
    }
}


