﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Homework_1
{
    class Stack16
    {

        public string Decode(string encodedStr)
        {
            string finalString = "";
            string encodedChunk = "";

            for (int i = 0; i < encodedStr.Length; i++)
            {

                if (i % 5 != 0 || i == 0)
                {
                    encodedChunk += encodedStr[i];
                }
                else
                {
                    finalString += Reverse(encodedChunk);
                    encodedChunk = encodedStr[i].ToString();
                }
                if (i == (encodedStr.Length - 1))
                {
                    finalString += Reverse(encodedChunk);
                }
            }

            return finalString;
        }
        public static string Reverse(string str)
        {
            char[] charArr = str.ToCharArray();
            int size = charArr.Length;
            Stack<char> stack = new Stack<char>();

            int i;
            for (i = 0; i < size; ++i)
            {
                stack.Push(charArr[i]);
            }

            for (i = 0; i < size; ++i)
            {
                charArr[i] = stack.Pop();
            }

            return new string(charArr);
        }
    }
}
