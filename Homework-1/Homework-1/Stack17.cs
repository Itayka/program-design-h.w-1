﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework_1
{
    class Stack17
    {
        public int GetLongestSeq(Stack<int> stack)
        {
            int curr = stack.Top();
            int next;
            int seqCount = 0;
            int maxSeqCount = 0;
            int i = 1;
            int stackSize = stack.Size();
            while (!stack.IsEmpty())
            {
                next = stack.Pop();
                if (curr == next + 1)
                {
                    seqCount++;
                }
                else
                {
                    if (seqCount > maxSeqCount)
                    {
                        maxSeqCount = seqCount;
                    }
                    seqCount = 0;
                }
                curr = next;

                if (i == stackSize)
                {
                    if (seqCount > maxSeqCount)
                    {
                        maxSeqCount = seqCount;
                    }
                }
                i++;
            }
            return maxSeqCount + 1;
        }
    }
}
