﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework_1
{
    class OOP1
    {

        public void PrintAvailableDishes(Menu menu, string missingFood)
        {
            menu.PrintAvailableDishes(missingFood);
        }
        public class Course
        {

            public int CourseCode { get; set; }
            public string CourseName { get; set; }
            public Node<string> Ingredients { get; set; }

            public Course(int courseCode, string courseName, Node<string> ingredients)
            {
                CourseCode = courseCode;
                CourseName = courseName;
                Ingredients = ingredients;
            }

        }

        public class Menu
        {
            public string Name { get; set; }
            public Course[] Courses { get; set; }

            public Menu(string name, Course[] courses)
            {
                Courses = new Course[22];
                Name = name;
                Courses = courses;
            }

            public void PrintAvailableDishes(string missingIngrident)
            {
                foreach (var course in Courses)
                {
                    bool good_dish = true;
                    Node<string> temp = course.Ingredients;
                    while (temp != null)
                    {
                        if (temp.GetValue() == missingIngrident)
                        {
                            good_dish = false;
                        }
                        temp = temp.GetNext();
                    }
                    if (good_dish)
                    {
                        Console.WriteLine(course.CourseCode);
                    }
                }
            }



        }
    }
}
