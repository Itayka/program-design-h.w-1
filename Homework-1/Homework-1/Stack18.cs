﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework_1
{
    class Stack18
    {
        public void Control(Stack<KeyValuePair<int, int>> stack)
        {

            Stack<int> s2 = new Stack<int>();


            int[] used = new int[stack.Size()];

            for (int i = 0; i < used.Length; i++)
            {
                used[i] = int.MaxValue;
            }

            for (int i = 0; i < stack.Size(); i++)
            {
                KeyValuePair<int, Stack<int>> value = GetArrangedStack(stack, s2, used);
                used[i] = value.Key;
                s2 = value.Value;
            }

            Console.WriteLine(s2.ToString());
        }

        public static KeyValuePair<int, Stack<int>> GetArrangedStack(Stack<KeyValuePair<int, int>> stack, Stack<int> orderdStack, int[] usedList)
        {
            KeyValuePair<int, int> value = GetMin(stack, usedList);
            for (int i = 0; i < value.Value; i++)
            {
                orderdStack.Push(value.Key);
            }
            return new KeyValuePair<int, Stack<int>>(value.Key, orderdStack);


        }

        public static KeyValuePair<int, int> GetMin(Stack<KeyValuePair<int, int>> stack, int[] usedList)
        {
            Stack<KeyValuePair<int, int>> temp = CopyValue(stack);
            KeyValuePair<int, int> min = new KeyValuePair<int, int>(int.MaxValue, int.MaxValue);


            while (!temp.IsEmpty())
            {
                KeyValuePair<int, int> next = temp.Pop();
                bool used = false;
                foreach (var item in usedList)
                {
                    if (item == next.Key)
                    {
                        used = true;
                        break;
                    }
                }
                if (next.Value < min.Value && !used)
                {

                    min = next;
                }
                stack.Push(next);
            }
            return min;
        }

        public static Stack<KeyValuePair<int, int>> CopyValue(Stack<KeyValuePair<int, int>> stack)
        {
            Stack<KeyValuePair<int, int>> temp = new Stack<KeyValuePair<int, int>>();
            while (!stack.IsEmpty())
            {
                temp.Push(stack.Pop());
            }
            return temp;

        }
    }
}
