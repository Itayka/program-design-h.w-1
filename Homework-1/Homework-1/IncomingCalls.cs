﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection.PortableExecutable;
using System.Text;



namespace Homework_1
{
    class IncomingCalls
    {
        private Node<string> list;

        public const int MAX = 5;

        private int currentCalls = 0;

        public IncomingCalls(Node<string> list, int currentCalls)
        {
            this.list = list;
            this.currentCalls = currentCalls;
        }

        public void getPhone(string phoneNum)
        {
            if (list is null)
            {
                list = new Node<string>(phoneNum);
            }
            else
            {
                Node<string> pos = list;
                bool found = false;
                int i = 0;


                while (!found && i < currentCalls)
                {
                    if (phoneNum.Equals(pos.GetValue()))
                    {
                        found = true;
                    }
                    else
                    {
                        i++;
                        pos = pos.GetNext();
                    }
                }
                if (!found)
                {
                    if (currentCalls < MAX)
                    {
                        currentCalls++;
                    }
                    else
                    {
                        pos = list;
                        while (pos.GetNext().GetNext() != null)
                        {
                            pos = pos.GetNext();
                        }
                        pos.SetNext(pos.GetNext().GetNext());
                    }
                }
                else
                {
                    if (list.GetValue().Equals(phoneNum))
                        list = list.GetNext();
                    else
                    {
                        pos = list;
                        while (pos.GetNext().GetValue().Equals(phoneNum) == false)
                            pos = pos.GetNext();
                        pos.SetNext(pos.GetNext().GetNext());
                    }
                }
                //Add new first number
                Node<string> temp = new Node<string>(phoneNum);
                temp.SetNext(list);
                list = temp;
                temp = null;
            }
        }

    }
}
