﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace Homework_1
{
    class Stack15
    {
        private Stack<int> temp = new Stack<int>();

        public KeyValuePair<int, bool> GetMinRecursive(Stack<int> stack, int minValue = int.MaxValue)
        {
            KeyValuePair<int, bool> result = new KeyValuePair<int, bool>(0, false);
            if (!stack.IsEmpty())
            {
                int next = stack.Pop();
                if (next < minValue)
                {
                    result = GetMinRecursive(stack, next);
                }
                else
                {
                    result = GetMinRecursive(stack, minValue);
                }
                temp.Push(next);
            }
            if (stack.IsEmpty())
            {
                if (!result.Value)
                {
                    int min = minValue;
                    return new KeyValuePair<int, bool>(min, true);
                }
                else
                {
                    return new KeyValuePair<int, bool>(result.Key, true);
                }
            }

            return new KeyValuePair<int, bool>();
        }

        public int GetMinIterative(Stack<int> stack)
        {
            int minValue = int.MaxValue;
            while (!stack.IsEmpty())
            {
                int next = stack.Pop();
                if (next < minValue)
                {
                    minValue = next;
                }
                temp.Push(next);
            }
            return minValue;
        }

        public void retriveStack(Stack<int> stack)
        {
            while (!temp.IsEmpty())
            {
                stack.Push(temp.Pop());
            }

        }

        public void arrangeStack(Stack<int> stack)
        {
            Stack<int> sortedStack = new Stack<int>();

            while (!stack.IsEmpty())
            {
                int minValue = GetMinIterative(stack);

                clearUsed(stack, minValue);
                sortedStack.Push(minValue);
            }

            clearStack(stack);
            while (!sortedStack.IsEmpty())
            {
                stack.Push(sortedStack.Pop());
            }
        }

        private void clearStack(Stack<int> stack)
        {
            while (!stack.IsEmpty())
            {
                stack.Pop();
            }
        }

        private void clearUsed(Stack<int> stack, int minValue)
        {
            while (!temp.IsEmpty())
            {
                int value = temp.Pop();

                if (minValue != value)
                {
                    stack.Push(value);
                }

            }

        }
    }

}


