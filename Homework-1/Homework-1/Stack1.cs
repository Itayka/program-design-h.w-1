﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Homework_1
{
    class Stack1
    {
        public int GetSumOfMaxPair(Stack<int> stack1, Stack<int> stack2)
        {
            int stack2MaxSum = GetSumOfMaxPairStack(stack2, 0, false);

            return GetSumOfMaxPairStack(stack1, stack2MaxSum, true);
        }

        public int GetSumOfMaxPairStack(Stack<int> stack2, int maxValue, bool stop)
        {
            int maxSum = maxValue;
            int next = 0;
            int curr = stack2.Top();
            Stack<int> temp = stack2;
            while (!temp.IsEmpty())
            {
                next = temp.Pop();
                if (curr + next > maxSum)
                {
                    maxSum = curr + next;
                    if (stop)
                    {
                        break;
                    }
                }

                curr = next;
            }
            return maxSum;
        }


    }
}
