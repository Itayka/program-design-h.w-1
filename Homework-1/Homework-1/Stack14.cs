﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace Homework_1
{
    class Stack14
    {
        public bool GetSeq(Stack<int> stack, int number)
        {
            string numStr = number.ToString();
            string StackStr = GetStackNumStr(stack);

            return StackStr.Contains(numStr) || StackStr.Contains(Reverse(numStr));
        }

        public static string Reverse(string s)
        {
            var result = new string(s.ToCharArray().Reverse().ToArray());
            return result;
        }

        public string GetStackNumStr(Stack<int> stack)
        {
            string str = "";

            while (!stack.IsEmpty())
            {
                str += stack.Pop();
            }
            return str;
        }
    }
}
