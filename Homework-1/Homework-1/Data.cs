﻿
using System.Collections.Generic;
using System.Data;

namespace Homework_1
{
    class Data
    {

        //singleton
        private static Data instance = null;
        private static readonly object padLock = new object();

        private Data()
        {
            fillStacks();
        }

        public static Data Instance
        {
            get
            {
                lock (padLock)
                {
                    if (instance == null)
                    {
                        instance = new Data();
                    }
                    return instance;
                }
            }
        }


        // ------------------------------------ task 53 ------------------------------------//

        private static Node<Node53.Grade> gradeItay =
                    new Node<Node53.Grade>(new Node53.Grade(8819516, 80),
                    new Node<Node53.Grade>(new Node53.Grade(8819517, 85),
                    new Node<Node53.Grade>(new Node53.Grade(8819518, 90))));



        private static Node<Node53.Grade> gradeMiki =
        new Node<Node53.Grade>(new Node53.Grade(8819516, 79.3),
        new Node<Node53.Grade>(new Node53.Grade(8819517, 87.4),
        new Node<Node53.Grade>(new Node53.Grade(8819518, 88.25))));




        public readonly Node<Node53.Student> GradeDB = new Node<Node53.Student>(new Node53.Student("Itay", 987264534, 2020, gradeItay),
                new Node<Node53.Student>(new Node53.Student("Miki", 487264534, 2020, gradeMiki)));
        // ------------------------------------ task 53 ------------------------------------//


        // ------------------------------------ task 48 ------------------------------------//

        public readonly Node<Node48.TupleStrInt> Representation =
                new Node<Node48.TupleStrInt>(new Node48.TupleStrInt("R", 1),
                new Node<Node48.TupleStrInt>(new Node48.TupleStrInt("L", 3),
                new Node<Node48.TupleStrInt>(new Node48.TupleStrInt("S", 4))));

        // ------------------------------------ task 48 ------------------------------------//


        // ------------------------------------ task 2 ------------------------------------//

        public readonly Node<int> Sequence =
                new Node<int>(3,
                new Node<int>(4,
                new Node<int>(5,
                new Node<int>(12,
                new Node<int>(19,
                new Node<int>(20,
                new Node<int>(100,
                new Node<int>(101,
                new Node<int>(102,
                new Node<int>(103,
                new Node<int>(104)))))))))));

        // ------------------------------------ task 2 ------------------------------------//


        // ------------------------------------ task 14 ------------------------------------//
        private static readonly Node14.Room[] Rooms = new Node14.Room[]
        {
            new Node14.Room("bedroom",1,1),
            new Node14.Room("bathroom",2,2),
            new Node14.Room("kitchen",3,3),
            new Node14.Room("basement",4,4),

            new Node14.Room("bedroom",1,2),
            new Node14.Room("bathroom",3,4),
            new Node14.Room("kitchen",5,6),
            new Node14.Room("basement",7,8),

            new Node14.Room("bedroom",10.1,10.4),
            new Node14.Room("bathroom",10.5,11.4),
            new Node14.Room("kitchen",10.4,10.12),
            new Node14.Room("basement",1,12.1),

            new Node14.Room("bedroom",1,1),
            new Node14.Room("bathroom",2,2),
            new Node14.Room("kitchen",3,3),
            new Node14.Room("basement",4,4),

            new Node14.Room("bedroom",1,2),
            new Node14.Room("bathroom",3,4),
            new Node14.Room("kitchen",5,6),
            new Node14.Room("basement",7,8),

            new Node14.Room("bedroom",10.1,10.4),
            new Node14.Room("bathroom",10.5,11.4),
            new Node14.Room("kitchen",10.4,10.12),
            new Node14.Room("basement",1,12.1),

            new Node14.Room("bedroom",1,2),
            new Node14.Room("bathroom",3,4),
            new Node14.Room("kitchen",5,6),
            new Node14.Room("basement",7,8),

            new Node14.Room("bedroom",10.1,10.4),
            new Node14.Room("bathroom",10.5,11.4),
            new Node14.Room("kitchen",10.4,10.12),
            new Node14.Room("basement",1,12.1),


        };

        private static readonly Node14.Address[] Addresses = new Node14.Address[]
        {
            new Node14.Address("moshe dayan","ramat-gan", 6),


            new Node14.Address("abarbanel","petah-tikva", 6),


            new Node14.Address("david grosman","jerusalem", 6),

            new Node14.Address("davidka","jerusalem", 6),

        };

        private static readonly Node14.Apartment[] Apartments = new Node14.Apartment[]
        {
            new Node14.Apartment("Itay", new Node14.Room[]{Rooms[0],Rooms[1],Rooms[2],Rooms[3]}),
            new Node14.Apartment("Miki", new Node14.Room[]{Rooms[4],Rooms[5],Rooms[6],Rooms[7]}),
            new Node14.Apartment("Zvi", new Node14.Room[]{Rooms[8],Rooms[9],Rooms[10],Rooms[11]}),
            new Node14.Apartment("Josef", new Node14.Room[]{Rooms[12],Rooms[13],Rooms[14],Rooms[15]}),
            new Node14.Apartment("Mika", new Node14.Room[]{Rooms[16],Rooms[17],Rooms[18],Rooms[19]}),
            new Node14.Apartment("Riki", new Node14.Room[]{Rooms[20],Rooms[21],Rooms[22],Rooms[23]}),
            new Node14.Apartment("Ella", new Node14.Room[]{Rooms[24],Rooms[25],Rooms[26],Rooms[27]}),
            new Node14.Apartment("Jake", new Node14.Room[]{Rooms[28],Rooms[29],Rooms[30],Rooms[31]}),

        };

        public readonly Node14.Building[] Buildings = new Node14.Building[]
        {
            new Node14.Building(new Node14.Apartment[] {Apartments[0],Apartments[1]},Addresses[0]),
            new Node14.Building(new Node14.Apartment[] {Apartments[2],Apartments[3]},Addresses[1]),
            new Node14.Building(new Node14.Apartment[] {Apartments[4],Apartments[5]},Addresses[2]),
            new Node14.Building(new Node14.Apartment[] {Apartments[6],Apartments[7]},Addresses[3]),


        };


        // ------------------------------------ task 14 ------------------------------------//

        // ------------------------------------ stack18 ------------------------------------//
        public readonly Stack<KeyValuePair<int, int>> stack18 = new Stack<KeyValuePair<int, int>>();

        // ------------------------------------ stack_18 ------------------------------------//

        // ------------------------------------ stack_1 ------------------------------------//
        public readonly Stack<int> stack1_1 = new Stack<int>();
        public readonly Stack<int> stack1_2 = new Stack<int>();
        // ------------------------------------ stack_1 ------------------------------------//
        // ------------------------------------ stack_14 ------------------------------------//

        public readonly Stack<int> stack14 = new Stack<int>();
        // ------------------------------------ stack_14 ------------------------------------//
        // ------------------------------------ stack_15 ------------------------------------//
        public readonly Stack<int> stack15 = new Stack<int>();

        // ------------------------------------ stack_15 ------------------------------------//

        // ------------------------------------ stack_16 ------------------------------------//
        public readonly string encodedString16 = "ollehf ym dneir era o uoy?k";

        // ------------------------------------ stack_16 ------------------------------------//


        // ------------------------------------ stack_17 ------------------------------------//
        public readonly Stack<int> stack17 = new Stack<int>();

        public void fillStacks()
        {
            stack18.Push(new KeyValuePair<int, int>(3, 0));
            stack18.Push(new KeyValuePair<int, int>(0, 4));
            stack18.Push(new KeyValuePair<int, int>(2, 5));
            stack18.Push(new KeyValuePair<int, int>(4, 6));

            stack1_1.Push(7);
            stack1_1.Push(6);
            stack1_1.Push(14);
            stack1_1.Push(8);
            stack1_1.Push(12);
            stack1_1.Push(9);
            stack1_1.Push(7);

            stack1_2.Push(11);
            stack1_2.Push(9);
            stack1_2.Push(1);
            stack1_2.Push(4);
            stack1_2.Push(13);
            stack1_2.Push(4);
            stack1_2.Push(8);
            stack1_2.Push(2);

            stack14.Push(1);
            stack14.Push(2);
            stack14.Push(3);
            stack14.Push(4);
            stack14.Push(5);
            stack14.Push(6);
            stack14.Push(7);
            stack14.Push(8);

            stack15.Push(2);
            stack15.Push(3);
            stack15.Push(1);
            stack15.Push(11);
            stack15.Push(5);
            stack15.Push(9);
            stack15.Push(7);
            stack15.Push(6);

            stack17.Push(2);
            stack17.Push(3);
            stack17.Push(4);
            stack17.Push(5);
            stack17.Push(6);
            stack17.Push(7);
            stack17.Push(8);
            stack17.Push(2);
            stack17.Push(7);
            stack17.Push(9);
            stack17.Push(33);
            stack17.Push(4);
            stack17.Push(11);
            stack17.Push(5);
            stack17.Push(1);
        }

        // ------------------------------------ stack_17 ------------------------------------//

        // ------------------------------------ OOP1 ------------------------------------//

        private static OOP1.Course Course1 = new OOP1.Course(1, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));
        private static OOP1.Course Course2 = new OOP1.Course(2, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course3 = new OOP1.Course(3, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course4 = new OOP1.Course(4, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course5 = new OOP1.Course(5, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course6 = new OOP1.Course(6, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course7 = new OOP1.Course(7, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course8 = new OOP1.Course(8, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course9 = new OOP1.Course(9, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course10 = new OOP1.Course(10, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course11 = new OOP1.Course(11, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course12 = new OOP1.Course(12, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course13 = new OOP1.Course(13, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course14 = new OOP1.Course(14, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course15 = new OOP1.Course(15, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course16 = new OOP1.Course(16, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course17 = new OOP1.Course(17, "a", new Node<string>("mango",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course18 = new OOP1.Course(18, "a", new Node<string>("chilli",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course19 = new OOP1.Course(19, "a", new Node<string>("peper",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course20 = new OOP1.Course(20, "a", new Node<string>("kiwi",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course21 = new OOP1.Course(21, "a", new Node<string>("yougort",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));

        private static OOP1.Course Course22 = new OOP1.Course(22, "a", new Node<string>("garlic",
        new Node<string>("onion",
        new Node<string>("tomato",
        new Node<string>("water",
        new Node<string>("cucumber",
        new Node<string>("chesse")))))));


        public readonly OOP1.Menu OOP1Menu = new OOP1.Menu("Mozes", new OOP1.Course[] { Course1, Course2, Course3, Course4, Course5, Course6, Course7, Course8, Course9, Course10, Course11, Course12, Course13, Course14, Course15, Course16, Course17, Course18, Course19, Course20, Course21, Course22});

        public readonly string MissingIngredientOOP1 = "garlic";
        // ------------------------------------ OOP1 ------------------------------------//

        private static OOP2.Contact Contact1 = new OOP2.Contact("itay", "0507144419");
        private static OOP2.Contact Contact2 = new OOP2.Contact("moshe", "0507144418");
        private static OOP2.Contact Contact3 = new OOP2.Contact("zvika", "0507144417");
        private static OOP2.Contact Contact4 = new OOP2.Contact("josef", "0507144416");
        private static OOP2.Contact Contact5 = new OOP2.Contact("ruhama", "0507144415");
        private static OOP2.Contact Contact6 = new OOP2.Contact("orit", "0507144414");
        private static OOP2.Contact Contact7 = new OOP2.Contact("yona", "0507144413");
        private static OOP2.Contact Contact8 = new OOP2.Contact("rahel", "0507144412");
        private static OOP2.Contact Contact9 = new OOP2.Contact("anna", "0507144411");
        private static OOP2.Contact Contact10 = new OOP2.Contact("liam", "0507144491");


        public readonly OOP2.PhoneBook phoneBook = new OOP2.PhoneBook(new Node<OOP2.Contact>(Contact3,
            new Node<OOP2.Contact>(Contact1,
            new Node<OOP2.Contact>(Contact2,
            new Node<OOP2.Contact>(Contact4,
            new Node<OOP2.Contact>(Contact6,
            new Node<OOP2.Contact>(Contact5,
            new Node<OOP2.Contact>(Contact7,
            new Node<OOP2.Contact>(Contact8,
            new Node<OOP2.Contact>(Contact10,
            new Node<OOP2.Contact>(Contact9)))))))))));


        // ------------------------------------ OOP2 ------------------------------------//


        // ------------------------------------ OOP2 ------------------------------------//











    }
}
